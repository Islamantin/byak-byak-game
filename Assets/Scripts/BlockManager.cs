﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BlockManager {
	[SerializeField]
	private GameObject blockPrefab;
	[SerializeField]
	private Transform parent;
	[SerializeField]
	private float newBlockOffset = 4f;
	private int maxBufferCapacity = 5;
	[SerializeField]
//	private List<BlockBehaviour> blockBuffer;
//	public BlockBehaviour LastCreatedBlock{ get { return blockBuffer [blockBuffer.Count - 1]; } }
//	private List<GameObject> blockBuffer;
	private Queue<GameObject> blockBuffer = new Queue<GameObject>();
	public GameObject LastCreatedBlock{ get; private set; }

	private BlockManager(){
	}

	public void PreinstantiateBlocksInPool(){
		var pooler = GameManager.Instance.pooler;
		for (int i = 0; i <= maxBufferCapacity; i++) {
			var go = GameObject.Instantiate (blockPrefab);
			pooler.AddWithTag (go, "block");
		}
	}

	public void ManageBlocks(Vector3 currentBlockPosition){
//		var o = GameObject.Instantiate (blockPrefab, currentBlockPosition + new Vector3 (0, newBlockOffset, 0), Quaternion.identity);
		var o = GameManager.Instance.pooler.GetByTag("block", currentBlockPosition + new Vector3 (0, newBlockOffset, 0), Quaternion.identity);
		o.transform.parent = parent;
		o.GetComponentInChildren<BlockTrigger> ().RaiseTriggerBack ();
//		var newbb = o.GetComponent<BlockBehaviour> ();
//		var tcs = newbb.GetTrapCreators ();
//		BuildTrapPattern (ref tcs);
//		newbb.SetTrapCreators (tcs);
//		newbb.Initialize ();
//		blockBuffer.Add (newbb);
		blockBuffer.Enqueue (o);
		LastCreatedBlock = o;

		GameManager.Instance.trapGenerator.GenerateTraps ();

		if (blockBuffer.Count > maxBufferCapacity) {
			var oldbb = blockBuffer.Peek();
			blockBuffer.Dequeue ();
			GameManager.Instance.pooler.AddWithTag (oldbb.gameObject, "block", 1f);
//			blockBuffer.TrimExcess ();
//			blockBuffer [0].deathT.gameObject.SetActive (true);
		}
	}
}

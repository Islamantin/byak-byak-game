﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CustomEvent : MonoBehaviour {

	public string acceptableKey;

	[SerializeField]
	private BaseEvent invokers;

	public void InvokeEvent(string accessKey){
		if (accessKey.Equals(acceptableKey))
		invokers.Invoke ();
	}

}

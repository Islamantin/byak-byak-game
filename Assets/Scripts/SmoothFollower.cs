﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothFollower : MonoBehaviour
{
	[SerializeField]
	private Transform target;
	[SerializeField]
	[Range(1,100)]
	private float speedFactor = 10f;
	[SerializeField]
	private bool constraintX;
	[SerializeField]
	private bool constraintY;
	[SerializeField]
	private bool constraintZ;
	private Vector3 velocity = Vector3.zero;
	private System.Action callback;
	
	// Update is called once per frame
	void Update ()
	{
		if (target != null) {
			var pos = transform.position;
			var tarPos = target.position;
			if (pos != tarPos) {
				if ((pos - tarPos).magnitude < 0.05f) {
					transform.position = tarPos;
					callback ();
					return;
				}
				if (constraintX)
					tarPos = new Vector3 (pos.x, tarPos.y, tarPos.z);
				if (constraintY)
					tarPos = new Vector3 (tarPos.x, pos.y, tarPos.z);
				if (constraintZ)
					tarPos = new Vector3 (tarPos.x, tarPos.y, pos.z);
				transform.position = Vector3.SmoothDamp (transform.position, tarPos, ref velocity, Time.deltaTime * 100 / speedFactor);
//				Debug.Log (velocity);
			}
		}
	}

	public void SetTarget(Transform newTarget, System.Action callback){
		target = newTarget;
		this.callback = callback;
	}

	public void ResetTarget(){
		target = null;
	}
}

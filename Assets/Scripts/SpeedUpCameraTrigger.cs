﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedUpCameraTrigger : MonoBehaviour
{

	[SerializeField]
	private MovingTowardsFollower followerToSpeedUp;
	[SerializeField]
	private float timeStep;
	[SerializeField]
	private float additionalSpeed;

	private JumperBehaviour target;
	private float initialSpeed;
	private float timeCounter = 0f;
	private bool exitStarted;

	void Start ()
	{
		initialSpeed = followerToSpeedUp.speedMultiplier;
	}

	void Update ()
	{
		if (target != null) {
			timeCounter += Time.deltaTime;
			if (timeCounter >= timeStep)
				followerToSpeedUp.speedMultiplier += additionalSpeed;
		} else if (exitStarted) {
			exitStarted = false;
			timeCounter = 0f;
			followerToSpeedUp.speedMultiplier = initialSpeed;
		}
	}

	void OnTriggerEnter (Collider other)
	{
		var jb = other.GetComponent<JumperBehaviour> ();
		if (jb != null) {
			target = jb;
		}
	}

	void OnTriggerExit (Collider other)
	{
		if (target == other.GetComponent<JumperBehaviour> ()) {
			target = null;
			exitStarted = true;
		}
	}
}

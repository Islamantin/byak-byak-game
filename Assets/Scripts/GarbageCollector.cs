﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GarbageCollector : MonoBehaviour {

	[SerializeField]
	private string prepoolEventKey = "prepool";

	void OnTriggerEnter (Collider other)
	{
		if (other.tag.Equals ("trap")) {
			var prepoolEvent = other.gameObject.GetComponent<CustomEvent> ();
			if (prepoolEvent != null)
				prepoolEvent.InvokeEvent(prepoolEventKey);
			GameManager.Instance.pooler.AddWithTag (other.gameObject, other.GetComponent<TrapID> ().type.ToString (), 0.5f);
		}
	}
}

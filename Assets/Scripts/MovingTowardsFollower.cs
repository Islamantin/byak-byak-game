﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingTowardsFollower : MonoBehaviour {

	[Range(0.1f,10f)]
	public float speedMultiplier = 2f;

	[SerializeField]
	private Transform target;
	[SerializeField]
	private bool constraintX;
	[SerializeField]
	private bool constraintY;
	[SerializeField]
	private bool constraintZ;
	
	// Update is called once per frame
	void Update () {
		if (target != null) {
			var pos = transform.position;
			var tarPos = target.position;
			if (pos != tarPos) {
				if ((pos - tarPos).magnitude < 0.05f) {
					transform.position = tarPos;
					return;
				}
				if (constraintX)
					tarPos = new Vector3 (pos.x, tarPos.y, tarPos.z);
				if (constraintY)
					tarPos = new Vector3 (tarPos.x, pos.y, tarPos.z);
				if (constraintZ)
					tarPos = new Vector3 (tarPos.x, tarPos.y, pos.z);
				speedMultiplier = speedMultiplier >= 10 ? 10 : speedMultiplier;
				transform.position = Vector3.MoveTowards (transform.position, target.position, Time.deltaTime * speedMultiplier);
			}
		}
	}

	public void SetTarget(Transform newTarget){
		target = newTarget;
	}

	public void ResetTarget(){
		target = null;
	}
}

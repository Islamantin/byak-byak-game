﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoNotDestroyScript : MonoBehaviour {

	private static bool dontDestroyFlag;

	void Awake() {
		if (dontDestroyFlag == false) {
			DontDestroyOnLoad(gameObject);
			dontDestroyFlag = true;
		} else gameObject.SetActive(false);
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffscreenDeathTrigger : MonoBehaviour
{
//	[SerializeField]
//	private DeathTriggerType type;

	void OnTriggerEnter (Collider other)
	{
		if (other.tag.Equals ("Player")) {
//			switch (type) {
//			case DeathTriggerType.OFFSCREEN:
				other.GetComponent<Rigidbody> ().isKinematic = true;
				Camera.main.GetComponent<MovingTowardsFollower> ().enabled = false;
            GameStatusManager.Instance.SetStatus(GameStatusManager.GameStatus.DEATH);
//				break;
//			case DeathTriggerType.SPIKES:
//				var jumper = other.GetComponent<JumperBehaviour> ();
//				if (!jumper.IsOnSurface) {
//					other.GetComponent<Rigidbody> ().isKinematic = true;
//					Camera.main.GetComponent<MovingTowardsFollower> ().enabled = false;
//					GameManager.Instance.uiManager.SetBool ("gameover", true);
//				}
//				break;
//			}
//		}
	}
//
//	private enum DeathTriggerType
//	{
//		OFFSCREEN,
//		SPIKES
	}
}

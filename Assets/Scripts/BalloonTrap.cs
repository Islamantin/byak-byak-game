﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalloonTrap : MonoBehaviour {

	[SerializeField]
	private float pushPower = 500;
//	[SerializeField]
//	private float offsetRange = 3.5f;
	[SerializeField]
	private LocalPositionOffseter offseter;
	[SerializeField]
	private ParticleSystem explosion;
	[SerializeField]
	private Animator animator;

	private Vector3 initialLocalPos;

	void Start(){		
		initialLocalPos = transform.localPosition;
		Prepare ();
	}

	void OnCollisionEnter(Collision collision){
		var rb = collision.collider.GetComponent<Rigidbody> ();
		if (rb!=null) {
			var jb = rb.GetComponent<JumperBehaviour> ();
			if (jb != null) {
//				explosion.Play ();
//				if (!jb.IsOnSurface) {
				animator.SetTrigger("bulb");
				jb.ForceTo(collision.contacts [0].normal*-pushPower);
//				}
//				gameObject.SetActive(false);
			}
		}
	}

	void Prepare(){
		offseter.DoOffset ();
//		transform.localPosition = new Vector3 (0.5f+Random.value * offsetRange, initialLocalPos.y+2f, initialLocalPos.z);
//		explosion.transform.position = transform.position;
	}

	public void Blow(){
		if (gameObject.activeSelf) {
			var p = transform.position;
			explosion.transform.position = new Vector3 (p.x, p.y, explosion.transform.position.z);
			explosion.Play ();
			gameObject.SetActive (false);
		}
	}

	public void Recover(){
		Prepare ();
		gameObject.SetActive(true);
	}
}

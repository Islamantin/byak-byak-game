﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagneticTrigger : MonoBehaviour {

	[SerializeField]
	private Transform magneticPoint;
	[SerializeField]
	private float forceFactor = 1f;
	private List<Rigidbody> targets = new List<Rigidbody>();

	void OnTriggerEnter(Collider other){
		var rb = other.GetComponent<Rigidbody> ();
		if (rb != null) {
			targets.Add (rb);
//			var cg = rb.GetComponent<CustomGravity> ();
//			if (cg!=null)
//				cg.active = false;
		}
	}

	void OnTriggerExit(Collider other){
		var rb = other.GetComponent<Rigidbody> ();
		if (rb != null) {
			targets.Remove (rb);
//			var cg = rb.GetComponent<CustomGravity> ();
//			if (cg!=null)
//				cg.active = true;
		}
	}

	void FixedUpdate(){
		foreach (var t in targets) {
			t.AddForce (forceFactor*(magneticPoint.position - t.transform.position), ForceMode.Acceleration);
		}
	}
}
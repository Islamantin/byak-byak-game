﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameStatusManager : MonoBehaviour {

    public static GameStatusManager Instance;

    private GameStatus currentStatus = GameStatus.START;
    public GameStatus CurrentStatus {
        get { return currentStatus; }
    }

    private void Awake()
    {
        Instance = this;
        ApplyStatus();
    }

    public void SetStatus(GameStatus gameStatus)
    {
        currentStatus = gameStatus;
        ApplyStatus();
    }

    private void ApplyStatus()
    {
        var gm = GameManager.Instance;
        switch (currentStatus)
        {
            case GameStatus.START:
                //gm.irisShotController.SetAlpha(1f);
                break;
            case GameStatus.PLAY:
                //gm.irisShotController.SetAlpha(1f);
                //gm.irisShotController.TweenAlpha(0f);
                gm.Jumper.Resurect();
                break;
            case GameStatus.REPLAY:
                //gm.irisShotController.SetAlpha(0f);
                //gm.irisShotController.TweenAlpha(1f);
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                GameManager.Instance.advertisements.ShowAdvertisement();
                break;
            case GameStatus.DEATH:
                gm.uiManager.GameOver();
                break;
        }
    }

    public enum GameStatus {
        START,
        PLAY,
        REPLAY,
        DEATH
    }
}

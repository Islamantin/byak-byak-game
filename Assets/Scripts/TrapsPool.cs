﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TrapsPool{
	[SerializeField]
	private TrapReference[] references;

	public TrapReference[] GetAllReferences(){
		return references;
	}

	public GameObject GetTrap(TrapType type){
		return System.Array.Find (references, x => x.type == type).prefab;
	}

	public TrapType GetRandomTrapTypeByWeights(){
		var sumW = 0;
		var treshold = 0f;
		var rnd = Random.value;
		var resultType = references [0].type; 
		foreach (var r in references) {
			sumW += r.weight;
			if (r.weight > 0)
				resultType = r.type;
		}
		for (int i=0; i<references.Length; i++) {
			var part = ((float) references[i].weight) / sumW;
			if (rnd > treshold && rnd <= treshold+part) {
				resultType = references [i].type;
				break;
			}
			treshold += part;
		}
		return resultType;
	}

	public TrapType GetRandomTrapTypeByWeightsExcepts(TrapType[] exceptTypes){
		var sumW = 0;
		var treshold = 0f;
		var rnd = Random.value;
		var resultType = references [0].type;
		List<TrapReference> exReferences = new List<TrapReference> ();
		foreach (var r in references) {
			if (!System.Array.Exists(exceptTypes, x => r.type==x)){
				exReferences.Add (r);
			}
		}
		foreach (var r in exReferences) {
			sumW += r.weight;
			if (r.weight > 0)
				resultType = r.type;
		}
		exReferences.TrimExcess ();
		for (int i=0; i<exReferences.Count; i++) {
			var part = ((float) exReferences[i].weight) / sumW;
			if (rnd > treshold && rnd <= treshold+part) {
				resultType = exReferences [i].type;
				break;
			}
			treshold += part;
		}
		return resultType;
	}
}

[System.Serializable]
public class TrapReference{
	public TrapType type;
	public int weight = 1;
	public GameObject prefab;
}
	
public enum TrapType
{
	NONE,
	SPIKES,
	BALLOON,
	MIDWALL,
	PORTAL
}

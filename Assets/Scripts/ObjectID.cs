﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectID : MonoBehaviour {
	[SerializeField]
	private string id;
	public string ID{ get { return id; } }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumperAnimationHub : MonoBehaviour {

	private Animator localAnimator;
	private Animator targetAnimator;

	// Use this for initialization
	void Start () {
		localAnimator = (Animator) GetComponent (typeof(Animator));
		targetAnimator = (Animator) GetComponentInChildren (typeof(Animator));
	}
	
	public void FlipLeft(){
		localAnimator.SetBool ("flip_left", true);
		localAnimator.SetTrigger ("flip");
	}

	public void FlipRight(){
		localAnimator.SetBool ("flip_left", false);
		localAnimator.SetTrigger ("flip");
	}

	public void Death(){
		localAnimator.SetBool ("flapflap", false);
		localAnimator.SetTrigger ("death");
	}

	public bool FlyStatus { get; private set; }

	public void Fly(bool status){
		FlyStatus = status;
		localAnimator.SetBool ("flapflap", status);
	}

//	public void FlipFlap(){
//		targetAnimator.SetTrigger ("flipflap");
//	}

//	IEnumerator DoFlip(bool toLeft){
//		
//	}
}

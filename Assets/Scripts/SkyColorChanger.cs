﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyColorChanger : MonoBehaviour {

	public float speedFactor = 1f;
	public int paletteStep = 2;
	public Color[] colorPalette;

//	private Material skybox;

//	private float timeScale = 1f;
//	private float timePassed = 0f;

//	private Color color1;
//	private Color color2;

	private int color1Index;
	private int nextColor1Index;
	private int color2Index;
	private int nextColor2Index;

	private float accumulator = 0f;

	// Use this for initialization
	void Start () {
		color1Index = Random.Range (0, colorPalette.Length - 1);
		nextColor1Index = color1Index + 1;
		color2Index = color1Index + paletteStep < colorPalette.Length ? color1Index + paletteStep : color1Index + paletteStep - colorPalette.Length;
		nextColor2Index = color2Index + 1 < colorPalette.Length ? color2Index + 1 : 0;
//		skybox = RenderSettings.skybox;
		RenderSettings.skybox.SetColor ("_Color1", colorPalette[color1Index]);
		RenderSettings.skybox.SetColor ("_Color2", colorPalette[color2Index]);
//		RenderSettings.skybox = skybox;
	}
	
	// Update is called once per frame
	void Update () {
//		timePassed += Time.deltaTime;
//		if (timePassed >= timeScale) {
//			timePassed = 0f;
//			var color1 = skybox.GetColor ("Color 1");
//			var color2 = skybox.GetColor ("Color 2");
//		}
		accumulator += Time.deltaTime*speedFactor;
		RenderSettings.skybox.SetColor ("_Color1", Color.Lerp(colorPalette[color1Index], colorPalette[nextColor1Index], accumulator));
		RenderSettings.skybox.SetColor ("_Color2", Color.Lerp(colorPalette[color2Index], colorPalette[nextColor2Index], accumulator));
//		RenderSettings.skybox = skybox;
//		Debug.Log(accumulator);
//		var c1 = skybox.GetColor ("_Color1");
		if (accumulator>=1f) {
			var arrLength = colorPalette.Length;
			color1Index = nextColor1Index;
			nextColor1Index = nextColor1Index + 1 < arrLength ? nextColor1Index + 1 : 0;
			color2Index = color2Index + 1 < arrLength ? color2Index + 1 : 0;
			nextColor2Index = nextColor2Index + 1 < arrLength ? nextColor2Index + 1 : 0;
			accumulator = 0;
		}
	}
}

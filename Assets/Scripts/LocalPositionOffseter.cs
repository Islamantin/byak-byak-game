﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalPositionOffseter : MonoBehaviour {
	
	[SerializeField]
	private Transform target;
	[SerializeField]
	private bool offsetOnStart = true;
	[SerializeField]
	private float xStart = 0.5f;
	[SerializeField]
	private float xOffsetRange = 3.5f;
	[SerializeField]
	private float yOffset = 0f;

	private Vector3 initialLocalPos;

	void Start(){
		initialLocalPos = transform.localPosition;
		if (offsetOnStart)
			DoOffset ();
	}

	public void DoOffset(){
		target.localPosition = new Vector3 (xStart+Random.value * xOffsetRange, initialLocalPos.y+yOffset, initialLocalPos.z);
	}
}

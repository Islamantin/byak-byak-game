﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalTrap : MonoBehaviour {

	[SerializeField]
	private float scaleReduceFactor = 10f;
	[SerializeField]
	private float pullSpeed = 5f;
	private JumperBehaviour target;

	void OnTriggerEnter(Collider other){
		switch (other.tag){
		case "Player":
			var jb = other.GetComponent<JumperBehaviour> ();
			if (!jb.IsOnSurface) {
				target = jb;
				StartCoroutine (DoPortal());
			}
			break;
		case "balloon":
			var balloon = other.GetComponent<BalloonTrap> ();
			balloon.Blow ();
			break;
		}
	}

	void OnTriggerExit(Collider other){
		if (target == other.GetComponent<JumperBehaviour> ()) {
			target = null;
		}
	}

	IEnumerator DoPortal(){
		yield return new WaitForSeconds (0.05f);
		if (target != null) {
			var tar = target;
			tar.Kill ();
			Camera.main.GetComponent<MovingTowardsFollower> ().enabled = false;

			var initScale = tar.transform.localScale;
			var targetScale = initScale / scaleReduceFactor;
            var targetPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z); //+ 1.5f);
			var t = 0f;
			while (t < 1f) {
				t += Time.deltaTime * pullSpeed;
				tar.transform.localScale = Vector3.MoveTowards (initScale, targetScale, t);
				tar.transform.position = Vector3.Lerp (tar.transform.position, targetPosition, t);
				yield return null;
			}
            yield return new WaitForSeconds (0.5f);
            GameStatusManager.Instance.SetStatus(GameStatusManager.GameStatus.REPLAY);
		}
	}
}

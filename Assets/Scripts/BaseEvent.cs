﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[System.Serializable]
public class BaseEvent : UnityEvent {
	public static BaseEvent operator+ (BaseEvent bEvent, UnityAction action){
		bEvent.AddListener(action);
		return bEvent;
	}

	public static BaseEvent operator- (BaseEvent bEvent, UnityAction action){
		bEvent.RemoveListener(action);
		return bEvent;
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikesDeathTrigger : MonoBehaviour {

	private JumperBehaviour target;

	void OnTriggerEnter(Collider other){
		switch (other.tag){
		case "Player":
			var jb = other.GetComponent<JumperBehaviour> ();
			if (!jb.IsOnSurface) {
				target = jb;
				StartCoroutine (WaitForKill ());
			}
			break;
		case "balloon":
			var balloon = other.GetComponent<BalloonTrap> ();
			balloon.Blow ();
			break;
		}
	}

	void OnTriggerExit(Collider other){
		if (target == other.GetComponent<JumperBehaviour> ()) {
			target = null;
		}
	}

	IEnumerator WaitForKill(){
		yield return new WaitForSeconds (0.05f);
		if (target!=null) {
			target.Kill (true);
			Camera.main.GetComponent<MovingTowardsFollower> ().enabled = false;
			yield return new WaitForSeconds (1f);
            GameStatusManager.Instance.SetStatus(GameStatusManager.GameStatus.DEATH);
		}
	}
}

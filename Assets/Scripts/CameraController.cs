﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour {
	[SerializeField]
	private float maxFov = 70;
	[Range(1, 10)]
	[SerializeField]
	private float fovChanginFactor = 10;
	private float initialFov;
	private bool increasingFov;

	private Camera cam;

	void Start(){
		cam = GetComponent<Camera> ();
		initialFov = cam.fieldOfView;
	}

	void Update(){
		if (!increasingFov) {
			if (cam.fieldOfView != initialFov) {
				cam.fieldOfView = Mathf.MoveTowards (cam.fieldOfView, initialFov, Time.deltaTime * fovChanginFactor);
			}
		}
	}

	void LateUpdate(){
		increasingFov = false;
	}

	public void IncreseFov(){
		increasingFov = true;
		cam.fieldOfView = Mathf.MoveTowards (cam.fieldOfView, maxFov, Time.deltaTime * fovChanginFactor);
	}
}

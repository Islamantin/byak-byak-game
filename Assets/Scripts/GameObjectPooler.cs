using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameObjectPooler : MonoBehaviour
{

	public Transform poolContainer;
	private List<GameObjectRef> pool = new List<GameObjectRef> ();

	public void AddWithTag (GameObject objectToPool, string tag)
	{	
		objectToPool.SetActive (false);
		objectToPool.transform.parent = poolContainer;
		objectToPool.transform.localPosition = Vector3.zero;
		pool.Add (new GameObjectRef (tag, objectToPool));
	}

	public void AddWithTag (GameObject objectToPool, string tag, float delay)
	{
		StartCoroutine (DoAddingWithDelay (objectToPool, tag, delay));
	}

	IEnumerator DoAddingWithDelay (GameObject objectToPool, string tag, float delay)
	{
		yield return new WaitForSeconds (delay);
		AddWithTag (objectToPool, tag);
	}

	public GameObject GetByTag (string tag, Vector3 position, Quaternion rotation)
	{
		try {
			var gor = pool.Find (x => x.tag.Equals (tag));
			pool.Remove (gor);
			var go = gor.gameObject;
			go.transform.parent = null;
			go.transform.position = position;
			go.transform.rotation = rotation;
			go.SetActive (true);
			return go;
		} catch (System.Exception ex) {
			Debug.Log (ex);
			Debug.Log (tag);
		}
		return null;
	}

	public class GameObjectRef
	{
		public readonly string tag;
		public readonly GameObject gameObject;

		public GameObjectRef (string tag, GameObject gameObject)
		{
			this.tag = tag;
			this.gameObject = gameObject;
		}
	}
}
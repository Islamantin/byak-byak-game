﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

[System.Serializable]
public class UIManager {
	[SerializeField]
	private Text scoreOutput;
	[SerializeField]
	private Text highScoreOutput;
	[SerializeField]
	private Animator animator;

	private UIManager(){
	}

	public void RefreshScore(){
		var gm = GameManager.Instance;
		var scoreText = gm.playerManager.Score.ToString();
		if (!scoreText.Equals (scoreOutput.text)) {
			animator.SetTrigger ("scorebulb");
			scoreOutput.text = gm.playerManager.Score.ToString();
		}
	}

	public void GameOver(){
		var gm = GameManager.Instance;
		var pm = gm.playerManager;
		highScoreOutput.text = pm.HighScore.ToString();
		animator.SetTrigger ("gameover");
		if (pm.HighScoreAchived)
			animator.SetTrigger ("highscore");
	}

    public void ShowMainMenu(bool show)
    {
        if (show)
        {
            animator.SetTrigger("showmenu");
        } else
        {
            animator.SetTrigger("hidemenu");
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Rigidbody))]
public class JumperBehaviour : MonoBehaviour
{
	private Rigidbody body;
	[SerializeField]
	private Vector2 jumpVector = new Vector2 (-1f, 1.5f);
	[SerializeField]
	private float jumpPower = 100f;
	[SerializeField]
	private float jumpHoldedPower = 10f;
	[SerializeField]
	private int jumpMaxHoldingTime = 100;
	private int currentJumpHoldingTime = 0;
	[SerializeField]
	[Range (0f, 10f)]
	private float wallSlideVelocity = 5f;
	[SerializeField]
	private ParticleSystem leftParticle;
	[SerializeField]
	private ParticleSystem rightParticle;
	[SerializeField]
	private float particleEmitRate = 1f;
	[SerializeField]
	private JumperAnimationHub animationHub;

	private Collision surfaceCollision;
	private bool isRightDirected;
	private bool allowedToSelectDirection;
	private bool isDead;

	public bool IsOnSurface{ get { return surfaceCollision != null; } }

	void Start ()
	{
		body = GetComponent<Rigidbody> ();
        Kill();
	}

	void Update ()
	{
		if (!isDead) {
			var inputPressed = Input.GetMouseButton (0) ? true : Input.touchCount > 0 ? true : false;
			var flyStatus = false;

			if (surfaceCollision != null) {
				var inputDown = true;
				var touchPos = new Vector3 ();
				if (Input.GetMouseButtonDown (0)) {
					touchPos = Input.mousePosition;
				} else if (Input.touchCount > 0) {
					touchPos = Input.GetTouch (0).position;
				} else
					inputDown = false;

				//			if (Input.GetKeyDown(KeyCode.Space)) {
				if (inputDown) {
					var vec = jumpVector;
					if (allowedToSelectDirection) {
						allowedToSelectDirection = false;
						var sign = touchPos.x >= Screen.width / 2 ? 1f : -1f;
						vec = new Vector2 (Mathf.Abs (jumpVector.x) * sign, jumpVector.y);
//					isRightDirected = sign > 0 ? false : true;
					}
//				if (isRightDirected)
//					animationHub.FlipLeft ();
//				else
//					animationHub.FlipRight ();
//				body.AddForce (vec * jumpPower, ForceMode.Impulse);
					ForceTo (vec * jumpPower);
				}
//		} else if (Input.GetKey (KeyCode.Space) && currentJumpHoldingTime > 0 && body.velocity.y > 0) {
			} else if (inputPressed && currentJumpHoldingTime > 0 /*&& body.velocity.y > 0*/) {
				if (currentJumpHoldingTime < jumpMaxHoldingTime - 5)
					flyStatus = true;
				body.AddForce (transform.up * jumpHoldedPower);
				currentJumpHoldingTime--;
			}
			if (animationHub.FlyStatus && !flyStatus) {
				animationHub.Fly (false);
			} else if (!animationHub.FlyStatus && flyStatus) {
				animationHub.Fly (true);
			}
		}
	}

	private float fixedCounter = 0f;

	void FixedUpdate ()
	{
		fixedCounter += Time.fixedDeltaTime;
		if (IsOnSurface) {
			var c = surfaceCollision.contacts;
			var normal = c [0].normal;
			if (normal.x != 0 && Mathf.Abs (normal.y) < 0.5f) {
				if (particleEmitRate <= fixedCounter && Mathf.Abs (body.velocity.y) >= 0.1f) {
					if (isRightDirected) {
						leftParticle.Emit (1);
					} else {
						rightParticle.Emit (1);
					}
					fixedCounter = 0f;
				}
				body.velocity = new Vector3 (-normal.x, -wallSlideVelocity, 0);
			}
		} //else if (!IsOnSurface) {
//			RaycastHit hit;
//			//Debug.DrawRay (animationHub.transform.position, animationHub.transform.right, Color.green, 2f);
//			var dir = Mathf.Sign(body.velocity.x);
//			if (Physics.Raycast (animationHub.transform.position, 
//				animationHub.transform.right*dir,
//				out hit, 3f)) {
//
//				if (hit.collider.tag.Equals ("surface")) {
//					if (dir>0)
//						animationHub.FlipLeft ();
//					else if (dir<0) animationHub.FlipRight ();
//				}
//			}
//		}
	}

	void OnCollisionEnter (Collision collision)
	{
		if (collision.collider.tag.Equals ("surface") && surfaceCollision == null) {
			currentJumpHoldingTime = jumpMaxHoldingTime;
			if (isRightDirected) {
//				animationHub.FlipLeft ();
				rightParticle.Play ();

			} else {
//				animationHub.FlipRight ();
				leftParticle.Play ();
			}
//			if (collision.contacts [0].normal.x == 0) {
//				animationHub.FlipRight ();
//			}
		}
	}

	void OnCollisionStay (Collision collision)
	{
		if (collision.collider.tag.Equals ("surface")) {
			surfaceCollision = collision;
			var normalX = collision.contacts [0].normal.x;

			allowedToSelectDirection = normalX == 0 ? true : false;

			var sign = Mathf.Sign (normalX);
			jumpVector = new Vector2 (sign * Mathf.Abs (jumpVector.x), jumpVector.y);

			if (sign < 0)
				ChangeDirection (false);
			else
				ChangeDirection (true);
		}
	}

	void ChangeDirection (bool toRight)
	{
		if (toRight) {
//			Debug.Log ("right");
			if (!isRightDirected)
				animationHub.FlipRight ();
		} else {
//			Debug.Log ("left");
			if (isRightDirected)
				animationHub.FlipLeft ();
		}
		isRightDirected = toRight;
	}

	void OnCollisionExit (Collision collision)
	{
		if (collision.collider.tag.Equals ("surface")) {
			surfaceCollision = null;
			if (isRightDirected) {
//				animationHub.FlipLeft ();
				leftParticle.Stop ();
			} else {
//				animationHub.FlipRight ();
				rightParticle.Stop ();
			}
		}
	}

	public void ForceTo (Vector3 force)
	{
		body.AddForce (force, ForceMode.Impulse);
		if (force.x >= 0) {
//			ChangeDirection (false);
//			if (isRightDirected) {
//				isRightDirected = false;
//			}
			animationHub.FlipLeft ();
		} else {
//			ChangeDirection (true);
//			if (!isRightDirected) {
//				isRightDirected = true;
//			}
			animationHub.FlipRight ();
		}
	}

	public void Kill (bool withAnimation = false)
	{
		isDead = true;
		if (withAnimation)
		animationHub.Death ();
		GetComponent<Rigidbody> ().isKinematic = true;
	}

    public void Resurect()
    {
        isDead = false;
        GetComponent<Rigidbody>().isKinematic = false;
    }
}

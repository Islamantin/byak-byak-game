﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerManager {

	[SerializeField]
	private string highScorePlayerPrefKey;

	public int Score { get; private set; }
	public int HighScore { get{return PlayerPrefs.GetInt (highScorePlayerPrefKey, 0);}}
	public bool HighScoreAchived { get; private set; }

	private PlayerManager(){
	}

	public void AddToScore(int additiveScore){
		if (additiveScore>0)
			Score += additiveScore;
		if (Score > PlayerPrefs.GetInt (highScorePlayerPrefKey, 0)) {
			HighScoreAchived = true;
			PlayerPrefs.SetInt (highScorePlayerPrefKey, Score);
		}
	}

	public void ClearHighScore(){
		PlayerPrefs.SetInt (highScorePlayerPrefKey, 0);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Advertisements;
using UnityEngine;

public class GameManager : MonoBehaviour {

    private static GameManager instance;
    public static GameManager Instance { get { return instance; } }

    public IrisShotController irisShotController;

    public BlockManager blockManager;
    public PlayerManager playerManager;
    public UIManager uiManager;
    public ResourceContainer resources;
    public TrapGenerator trapGenerator;
    public GameObjectPooler pooler;
    public AdvertisementManager advertisements;

    public JumperBehaviour Jumper { get; private set; }

    [SerializeField]
    private bool clearPrefs;

    void Awake() {
        // Screen.SetResolution (270, 480, false);	
        instance = this;

        blockManager.PreinstantiateBlocksInPool();
        trapGenerator.InstantiateAllTrapsInPool();
        Jumper = GameObject.FindGameObjectWithTag("Player").GetComponent<JumperBehaviour>();
        //		blockManager.Initialize ();
    }

    private void Start()
    {
        if (!Advertisement.isShowing)
        GameStatusManager.Instance.SetStatus(GameStatusManager.GameStatus.PLAY);
    }

    void OnEnable() {
        if (clearPrefs) {
            clearPrefs = false;
            PlayerPrefs.DeleteAll();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockTrigger : MonoBehaviour {
	[SerializeField]
	private Transform cameraAnchor;
	[SerializeField]
	private Transform cameraNextAnchor;
	[SerializeField]
	private GameObject line;
	private bool triggered;


	void OnTriggerEnter(Collider other){
		if (other.tag.Equals ("Player") && !triggered) {
			triggered = true;
			var gm = GameManager.Instance;
			gm.blockManager.ManageBlocks (transform.position);
			gm.playerManager.AddToScore (1);
			gm.uiManager.RefreshScore();
//			Camera.main.GetComponent<MovingTowardsFollower> ().ResetTarget ();
//			Camera.main.GetComponent<SmoothFollower> ().SetTarget (cameraAnchor, SwitchFollowers);
			Camera.main.GetComponent<MovingTowardsFollower> ().SetTarget (cameraNextAnchor);
			line.SetActive (false);
		}
	}

	public void RaiseTriggerBack(){
		line.SetActive (true);
		triggered = false;
	}

//	void SwitchFollowers(){
//		Camera.main.GetComponent<SmoothFollower> ().ResetTarget ();
//		Camera.main.GetComponent<MovingTowardsFollower> ().SetTarget (cameraNextAnchor);
//	}
}

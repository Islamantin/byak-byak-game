﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TrapGenerator
{
	public float minTrapDistance = 2f;
	public float maxTrapDistance = 4f;
	public float maxGenerationDistance = 10f;
	//	public int trapsInARow = 6;
	public int bufferMaxSize = 6;
	public Transform leftTrapAnchor;
	public Transform rightTrapAnchor;
	public List<TrapTypeAndTimes> excludeFromReappearence;

	private List<TrapID> lastTraps = new List<TrapID> ();

	private TrapID LastTrap { get { return lastTraps.Count > 0 ? lastTraps [lastTraps.Count - 1] : null; } }

	private TrapType lastTrapType;
	private bool leftAnchor;

	private TrapGenerator ()
	{
	}

	public void InstantiateAllTrapsInPool ()
	{
		var trapsPool = GameManager.Instance.resources.trapsPool;
		var eachTrapTypeInPoolCount = bufferMaxSize * 2;
		var pooler = GameManager.Instance.pooler;
		foreach (var r in trapsPool.GetAllReferences()) {
			if (r.weight > 0) {
				for (int i = 0; i < eachTrapTypeInPoolCount; i++) {
					var go = GameObject.Instantiate (r.prefab);
					pooler.AddWithTag (go, r.type.ToString ());
				}
			}
		}
	}

	public void GenerateTraps ()
	{
		if (Random.value > 0.5f)
			leftAnchor = true;
//		var trapsCount = trapsInARow;
		var distanceCounter = LastTrap == null ? 0 : LastTrap.transform.position.y - Camera.main.transform.position.y;
		var pooler = GameManager.Instance.pooler;
		while (distanceCounter < maxGenerationDistance) {
//		while (trapsCount > 0) {
			var targetAnchor = rightTrapAnchor;
			if (leftAnchor)
				targetAnchor = leftTrapAnchor;
			
			var trapsPool = GameManager.Instance.resources.trapsPool;
			var trapType = trapsPool.GetRandomTrapTypeByWeights ();
//			var trap = trapsPool.GetTrap (trapType);

			Vector3 newTrapPos = targetAnchor.position;
			var trapOffset = new Vector3 (0, Random.Range (minTrapDistance, maxTrapDistance), 0);

			if (lastTraps.Count > 0) {
//				if (lastTrapType == TrapType.MIDBUSTER || lastTrapType == TrapType.MIDWALL) {
//					if (trapType == TrapType.BUSTER || trapType == TrapType.SPIKES) {
//						trapOffset += new Vector3 (0, minTrapDistance, 0);
//					}
//				}
//				newTrapPos = new Vector3 (targetAnchor.position.x, lastTraps [lastTraps.Count - 1].position.y, 0) + trapOffset;
//			}
				switch (lastTrapType) {
				case TrapType.MIDWALL:
					switch (trapType) {
//					case TrapType.NONE:
//						break;
					case TrapType.MIDWALL:
						var y = minTrapDistance;
//						if (Random.value >= 0.25f)
//							y = maxTrapDistance*2f;
						trapOffset = new Vector3 (0, y, 0);
						break;
//					case TrapType.MIDBUSTER:
////						trapOffset = new Vector3 (0, minTrapDistance, 0);
//						break;
//					default:
//						trapOffset += new Vector3 (0, minTrapDistance, 0);
//						break;
					}
					break;
//				case TrapType.PORTAL:
//					switch (trapType) {
//					case TrapType.PORTAL:
//						TrapType[] excepts = { TrapType.PORTAL };
//						trapType = trapsPool.GetRandomTrapTypeByWeightsExcepts (excepts);
//						break;
//					}
//					break;
//				case TrapType.MIDBUSTER:
//					switch (trapType) {
//					case TrapType.NONE:
//						break;
//					case TrapType.MIDWALL:
////						trapOffset = new Vector3 (0, minTrapDistance, 0);
//						break;
//					case TrapType.MIDBUSTER:
////						trapOffset = new Vector3 (0, maxTrapDistance, 0);
//						break;
//					default:
//						trapOffset += new Vector3 (0, maxTrapDistance, 0);
//						break;
//					}
//					break;
//				case TrapType.BUSTER:
//					switch (trapType) {
//					case TrapType.NONE:
//						break;
//					case TrapType.MIDWALL:
//						break;
//					default:
//						trapOffset += new Vector3 (0, maxTrapDistance, 0);
//						break;
//					}
//					break;
				}
				var excl = excludeFromReappearence.Find (x => x.type == trapType);
				if (excl!=null){
					var bufferSize = lastTraps.Count;
					var countUpTo = excl.times < bufferSize ? excl.times : bufferSize;
					for (int i = 0; i<countUpTo; i++) {
						if (lastTraps [bufferSize - 1 - i].type == trapType) {
							TrapType[] excepts = { trapType };
							trapType = trapsPool.GetRandomTrapTypeByWeightsExcepts (excepts);
							break;
						}
					}
				}
				newTrapPos = new Vector3 (targetAnchor.position.x, LastTrap.transform.position.y, 0) + trapOffset;
			}


//			var go = GameObject.Instantiate (trap, newTrapPos, targetAnchor.localRotation);
			var go = pooler.GetByTag (trapType.ToString (), newTrapPos, targetAnchor.localRotation);
			switch (trapType) {
			case TrapType.BALLOON:
				go.GetComponentInChildren<BalloonTrap> (true).Recover ();
				break;
			}

			lastTraps.Add (go.GetComponent<TrapID> ());
			if (lastTraps.Count > bufferMaxSize) {
				lastTraps.RemoveAt (0);
				lastTraps.TrimExcess ();
			}

			lastTrapType = trapType;
//			--trapsCount;
			distanceCounter += trapOffset.y;
			leftAnchor = !leftAnchor;
		}
	}

	[System.Serializable]
	public class TrapTypeAndTimes
	{
		public TrapType type;
		public int times;
	}
}

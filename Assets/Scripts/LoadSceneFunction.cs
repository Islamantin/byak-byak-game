﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LoadSceneFunction : MonoBehaviour {

	[SerializeField]
	private string sceneName;
	[SerializeField]
	private string invokerKey;
	private bool pressed;

	void Update(){
		if (Input.GetKeyDown (invokerKey) && !pressed) {
			pressed = true;
			LoadScene ();
		}
	}

	public void LoadScene(){
		SceneManager.LoadScene (sceneName);
	}
}

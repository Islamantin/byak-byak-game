﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteOffseter : MonoBehaviour {

	public SpriteRenderer targetRenderer;
	public Vector2 offsetFactor;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (targetRenderer != null) {
			var offset = targetRenderer.material.mainTextureOffset;
			var t = Time.deltaTime;
			targetRenderer.material.mainTextureOffset = new Vector2 (offset.x+ t*offsetFactor.x, offset.y + t*offsetFactor.y);
		}
	}
}

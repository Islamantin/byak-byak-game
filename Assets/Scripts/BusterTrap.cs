﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BusterTrap : MonoBehaviour {

	[SerializeField]
	private float pushPower = 500;

//	void Start(){
//		var s = transform.localScale;
//		transform.localScale = new Vector3 (s.x, Random.Range (0.5f, 1.5f), s.z);
//	}

	void OnCollisionEnter(Collision collision){
		var rb = collision.collider.GetComponent<Rigidbody> ();
		if (rb!=null) {
			var jb = rb.GetComponent<JumperBehaviour> ();
			if (jb != null) {
				if (!jb.IsOnSurface) {
					rb.AddForce (collision.contacts [0].normal*-pushPower, ForceMode.Impulse);
				}
			}
		}
	}
}

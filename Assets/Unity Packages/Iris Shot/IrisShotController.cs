﻿using UnityEditor;
using UnityEngine;

// main script to control iris shot effect
// needs PostEffector to get/set params from target material
[ExecuteInEditMode]
[RequireComponent(typeof(PostEffector))]
public class IrisShotController : MonoBehaviour
{
    private PostEffector postEffector;

    [Range(0f, 1f)]
    [SerializeField]
    private float alpha; // intencity of blackness outside the shape
    [Range(0f, 1f)]
    [SerializeField]
    private float scale; // scale of shape on screen
    [SerializeField]
    private Vector2 centerPosition; // position of shape on screen
    [SerializeField]
    private bool autoSizeProportionCorrection; // you'll need it true only if you frequently changing size of game screen (in editor, for example)
    [SerializeField]
    private Vector2 sizeProportion; // value of shape size proportion relative to screen size

    void Awake()
    {
        postEffector = gameObject.GetComponent<PostEffector>();
        var camera = GetComponent<Camera>();
        if (camera != null)
        {
            // everything necessary is done automatically for you when you attach this script to gameobject
            if (Application.isEditor)
            {
                // canvases needs to be not in 'Screen Space - Overlay' mode, because of rendering order 
                // (ui elements are draws over screen effects)
                // and here all the canvases switches on Camera mode
                var canvases = FindObjectsOfType<Canvas>();
                foreach (var c in canvases)
                {
                    if (c.renderMode == RenderMode.ScreenSpaceOverlay)
                    {
                        c.renderMode = RenderMode.ScreenSpaceCamera;
                        c.worldCamera = camera;
                        Debug.LogWarning("Renderer mode of canvas \"" + c.gameObject.name
                            + "\" was changed from \'Screen Space - Overlay\' to \'Screen Space - Camera\'.");
                    }
                }

                // you don't need to set necessary material by yourself
                if (postEffector.material == null)
                {
                    // it just finds material in assets
                    var path = AssetDatabase.GUIDToAssetPath(AssetDatabase.FindAssets("IrisShotMaterial l:IrisShot t:Material")[0]);
                    postEffector.material = AssetDatabase.LoadAssetAtPath<Material>(path);
                }
            }

            // getting values from material
            MapValuesFromMaterial();
            // subscribing to event which invokes when image size will change 
            postEffector.TextureSizeChanged += DoSizeProportionCorrection;
        } else
        {
            GameObject.DestroyImmediate(this);
        }
    }

    // invokes when any value of this script changed in inspector
    // just immediate applies new values to material in editor
    private void OnValidate()
    {
        if (postEffector!=null)
        {
            SetAlpha(alpha);
            SetScale(scale);
            SetCenterPosition(centerPosition);
            if (!autoSizeProportionCorrection)
                SetSizeProportion(sizeProportion);
            else DoSizeProportionCorrection();
        }
    }

    // calculating quad proportion of shape size 
    public Vector2 CalculateCorrectSizeProportion(Vector2 textureSize)
    {
        var r = new Vector2(textureSize.x, textureSize.y);
        if (r.x > r.y)
        {
            r = new Vector2(1f, r.y / r.x);
        }
        else
        {
            r = new Vector2(r.x / r.y, 1f);
        }
        return r;
    }

    // interface to set new position of shape
    public void SetCenterPosition(Vector2 position)
    {
        postEffector.material.SetFloat("_CenterX", position.x);
        postEffector.material.SetFloat("_CenterY", position.y);
    }

    // interface to set new size of shape
    public void SetScale(float size)
    {
        postEffector.material.SetFloat("_Scale", size*1.3f);
    }

    // interface to set new blackness intencity
    public void SetAlpha(float alpha)
    {
        postEffector.material.SetFloat("_Alpha", alpha);
    }

    // interface to set new size proportion of shape
    public void SetSizeProportion(Vector2 proportion)
    {
        postEffector.material.SetFloat("_HorizontalProp", proportion.x);
        postEffector.material.SetFloat("_VerticalProp", proportion.y);
    }

    // manual size proportion correction when clicking it in context menu of component in inspector
    [ContextMenu("Do Ratio Correction")]
    public void DoSizeProportionCorrection()
    {
        sizeProportion = CalculateCorrectSizeProportion(postEffector.TextureSize);
        SetSizeProportion(sizeProportion);
    }

    // auto size proportion correction - event handler
    private void DoSizeProportionCorrection(object sender, TextureSizeChangedEventArgs e)
    {
        if (autoSizeProportionCorrection)
        {
            sizeProportion = CalculateCorrectSizeProportion(e.NewTextureSize);
            SetSizeProportion(sizeProportion);
        }
    }

    // getting parameter values from material parameter values
    // can be invoked from component context menu in inspector
    [ContextMenu("Map Values From Material")]
    public void MapValuesFromMaterial()
    {
        alpha = postEffector.material.GetFloat("_Alpha");
        scale = postEffector.material.GetFloat("_Scale")/1.3f;
        var cx = postEffector.material.GetFloat("_CenterX");
        var cy = postEffector.material.GetFloat("_CenterY");
        centerPosition = new Vector2(cx, cy);
        var hp = postEffector.material.GetFloat("_HorizontalProp");
        var vp = postEffector.material.GetFloat("_VerticalProp");
        sizeProportion = new Vector2(hp, vp);
    }

    private void OnDestroy()
    {
        if (postEffector)
            postEffector.TextureSizeChanged -= DoSizeProportionCorrection;
    }
}
